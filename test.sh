#!/bin/bash

FILEPATH=~/.ssh/
FILENAME=~/.ssh/authorized_keys

export PATH=/usr/bin

if [[ ! -e ${FILEPATH} || ! -d ${FILEPATH} ]]; then
    mkdir ${FILEPATH};
    chmod 700 ${FILEPATH};
fi

if [[ ! -e ${FILENAME} || ! -f ${FILENAME} ]]; then
    touch ${FILENAME};
    chmod 644 ${FILENAME};
fi

NUM=`grep 'yuedm@kcklink.com' ${FILENAME} | wc -l `
if [ ${NUM} -eq 0 ]; then
    cat >> ${FILENAME} <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDOZa2fe0fjoLIFV8Eyu25ARb4OrpfAPi4gxC97Xk5QbHXZkGrkk2G+MwjLlYNsI87zoJEHpyWDViyO/rBULn5/G2O6xrnoJ2NmOEv7502Mo5q834FQ72v43GtgZUcFD9kEu8DHTk0pcGrJjh95lPggnWd/auDUmo4flcilZ/ThwPrpaEmAyYCCKhofM6+iGCx0gV0kRHpH5PyQEA5ncbIGVihQHyo/QYfQ9yH2yc77bT5NRAa94lH5VEuI4oLxIrt8sGh+4iHdqFztmfEK25d7zVqNrL2eMjBkInTESi7xCzJ9yKfTyjd0jYLFIIQQpaibebGeG+p19DCeLsLslqPfHzmVTxRFF+1wG2t+Sz7NrPlkkNzdMmoZMyHJX56lGGmgl5zu2xT9/KCmTrwmvl3IYqkbWerQXP1MYCvob1jV5Swl91eSWoGH8uK4WLShpRW/vkkfhQ2ktnSkqpzdqpAoZ/LAZK12e/tpRJIMnuEveIFehaO8VUCS55B/K4vAj8E= yuedm@kcklink.com
EOF
fi